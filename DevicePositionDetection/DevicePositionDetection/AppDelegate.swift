//
//  AppDelegate.swift //  DevicePositionDetection
//
//  Created by Chris Watts on 13/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: DevicePositionDetectionExample
import UIKit
import NumberEight

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    private let ne = NumberEight()

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY", launchOptions: launchOptions, consentOptions: ConsentOptions.withConsentToAll())

        self.applicationWillEnterForeground(application)
        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

        ne.devicePositionUpdated { (glimpse) in
            let position = glimpse.mostProbable
            DispatchQueue.main.async {
                print("iOSExample: \(position.description)")
            }
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        ne.unsubscribeFromAll()
    }
}
/// End CodeSnippet: DevicePositionDetectionExample
