//
//  AppDelegate.m
//  DevicePositionDetectionObjc
//
//  Created by Oliver Kocsis on 14/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//


/// Begin CodeSnippet: DevicePositionDetectionExample
#import "AppDelegate.h"
@import NumberEight;

@interface AppDelegate ()

@property (nonatomic, strong) NEXNumberEight* ne;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.ne = [[NEXNumberEight alloc] init];

    [NEXNumberEight startWithApiKey:@"REPLACE_WITH_DEVELOPER_KEY"
                      launchOptions:launchOptions
                     consentOptions:[NEXConsentOptions withConsentToAll]
                         completion:nil];

    [self applicationWillEnterForeground:application];
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self.ne subscribeToDevicePosition:^(NEXGlimpse<NEXDevicePosition *> * _Nonnull glimpse) {
        NEXDevicePosition *position = glimpse.mostProbable;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"iOSExample: %@", position.description);
        });
    }];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self.ne unsubscribeFromAll];
}

@end
/// End CodeSnippet: DevicePositionDetectionExample
