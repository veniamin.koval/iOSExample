//
//  AppDelegate.swift
//  ContextMusic
//
//  Created by Chris Watts on 13/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: ContextMusicExample
import UIKit
import NumberEight
import NumberEightCompiled

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    private let ne = NumberEight()

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY", launchOptions: launchOptions, consentOptions: ConsentOptions.withConsentToAll())

        print("iOSExample: Waiting for situation updates...")
        ne.subscribe(to: kNETopicSituation, parameters: .changesMostProbableOnly) {
            (glimpse : Glimpse<NEXSituation>) in
            let major = glimpse.mostProbable.value.major
            if let tags = kTagLookup[major] {
                let playlist = self.getPlaylist(tags: tags)

                let text = "New playlist: \(playlist.joined(separator: ","))"
                print("iOSExample: \(text)")
            }
        }

        return true
    }

    func getPlaylist(tags: [String]) -> [String] {
        return Array(repeating: "Rick Astley - Never Gonna Give You Up", count: 10)
    }
}

let kTagLookup = [
    NESituationMajor.travelling: ["steampunk", "dj",
    "driving", "progressive", "feel good", "groovy", "roadtrip", "super", "funky",
    "psycheledic"],
    NESituationMajor.housework: ["energetic", "catchy",
    "happy", "sweet", "guilty pleasure", "smile", "sing along", "positive", "fun",
    "quirky", "ambient"],
    NESituationMajor.leisure: ["relaxing",
    "atmospheric", "easy listening", "soothing", "lounge", "chill", "summer",
    "uplifting", "chillout", "beautiful", "beach", "sunny"],
    NESituationMajor.morningRituals: ["summer",
    "upbeat", "feel good", "awesome", "chill", "morning", "happy", "amazing",
    "acoustic", "fun"],
    NESituationMajor.shopping: ["steampunk", "dj",
    "driving", "progressive", "feel good", "groovy", "roadtrip", "super", "funky",
    "psycheledic"],
    NESituationMajor.sleeping: ["relaxing", "calm",
    "mellow", "dreamy", "sad", "beautiful", "ballad", "meditative", "peaceful",
    "soothing", "relaxation", "instrumental"],
    NESituationMajor.social: ["dance", "party", "pop",
    "upbeat", "club", "schlager", "house", "summer", "remix", "electro"],
    NESituationMajor.working: ["chill", "classical",
    "piano", "instrumental", "new age", "ambient", "beautiful", "mellow", "relax",
    "soundtrack", "composer", "studying", "working"],
    NESituationMajor.workingOut: ["dance",
    "electronic", "gym", "house", "fun", "club", "party", "mix", "mashups",
    "workout", "motivation", "running", "cardio"]
]
/// End CodeSnippet: ContextMusicExample
