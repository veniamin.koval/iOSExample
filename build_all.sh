#!/bin/bash
set -euo pipefail

xcpretty_shell='cat -'
if command -v $(which xcpretty) &> /dev/null; then
    xcpretty_shell="xcpretty";
fi

if command -v $(which pod) &> /dev/null; then
	echo "Found Cocoapods"
else
	echo "Failed to find Cocoapods.  Install with: gem install cocoapods"
	exit 1
fi

if command -v $(which xcodebuild) &> /dev/null; then
	echo "Found Xcode"
else
	echo "Failed to find Xcode.  Install from the App Store"
	exit 1
fi

function xcbuild {
	target=${1}
	config=${2}
	sdk=${3}
	xcodebuild -workspace iOSExample.xcworkspace -scheme $target -sdk ${sdk} CONFIGURATION=${config} ONLY_ACTIVE_ARCH=NO RUN_CLANG_STATIC_ANALYZER=YES build | "${xcpretty_shell}"
}


rm -f Podfile.lock
arch -x86_64 pod update
all_schemes=`xcodebuild -workspace iOSExample.xcworkspace -list | grep -E "^        *" | grep "Example"`

num_schemes=0
for target in $all_schemes; do
	echo "Building Target: ${target} (Device/Simulator) Release/Debug"
	((num_schemes+=1))
done
echo "Building ${num_schemes} targets"

i=0
for target in $all_schemes; do
	echo "Build Target (${i}.0 / ${num_schemes}): ${target} (Device) Release"
	xcbuild ${target} Release iphoneos
	echo "Build Target (${i}.25 / ${num_schemes}): ${target} (Simulator) Release"
	xcbuild ${target} Release iphonesimulator

	echo "Build Target (${i}.50 / ${num_schemes}): ${target} (Device) Debug"
	xcbuild ${target} Debug iphoneos
	echo "Build Target (${i}.75 / ${num_schemes}): ${target} (Simulator) Debug"
	xcbuild ${target} Debug iphonesimulator
	((i+=1))
done

for target in $all_schemes; do
	echo "Built Target: ${target} (Device/Simulator) Release/Debug"
done
