import NumberEight

class MyClass {
/// Begin CodeSnippet: NumberEightGettingStarted
func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY",
                      launchOptions: launchOptions,
                      consentOptions: ConsentOptions.withConsentToAll())
    return true
}
/// End CodeSnippet: NumberEightGettingStarted
}
