//
//  ViewController.swift
//  MainExample
//
//  Created by Chris Watts on 12/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

import UIKit
import NumberEight

class ViewController: UIViewController {
    let pink = UIColor(red: 1.0, green: 0.251, blue: 0.506, alpha: 1.0)
    
    @IBOutlet weak var activityName: UILabel!
    @IBOutlet weak var activityImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let ne = NumberEight()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        activityImage.tintColor = self.pink
        activityIndicator.style = .whiteLarge
        activityIndicator.color = pink
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        
        ne.activityUpdated { (glimpse) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.activityName.text = glimpse.mostProbable.description
                
                switch (glimpse.mostProbable.value.state) {
                case .stationary:
                    self.activityImage.image = UIImage(named: "ic_airline_seat_recline_normal_48pt")
                    break
                case .walking:
                    self.activityImage.image = UIImage(named: "ic_directions_walk_48pt")
                    break
                case .running:
                    self.activityImage.image = UIImage(named: "ic_directions_run_48pt")
                    break
                case .cycling:
                    self.activityImage.image = UIImage(named: "ic_directions_bike_48pt")
                    break
                case .inVehicle:
                    self.activityImage.image = UIImage(named: "ic_directions_car_48pt")
                    break
                default:
                    self.activityName.text = "..."
                    self.activityImage.image = nil
                    self.activityIndicator.startAnimating()
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ne.unsubscribeFromAll()
    }
    
}

