//
//  Snippets.m
//  MainExample
//
//  Created by Chris Watts on 12/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/// Begin CodeSnippet: ImportNumberEight
@import NumberEight;
/// End CodeSnippet: ImportNumberEight

@interface AppDelegateExample2 : UIResponder<UIApplicationDelegate>
@end

@implementation AppDelegateExample2
/// Begin CodeSnippet: NumberEightGettingStarted
-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [NEXNumberEight startWithApiKey:@"REPLACE_WITH_DEVELOPER_KEY"
                      launchOptions:launchOptions
                     consentOptions:[NEXConsentOptions withConsentToAll]
                         completion:nil];
    return YES;
}
/// End CodeSnippet: NumberEightGettingStarted
@end

@interface AppDelegateExample : UIResponder <UIApplicationDelegate>
@end

@implementation AppDelegateExample

/// Begin CodeSnippet: NumberEightGettingStartedAuthorization
-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [NEXNumberEight startWithApiKey:@"REPLACE_WITH_DEVELOPER_KEY"
                      launchOptions:launchOptions
                     consentOptions:[NEXConsentOptions withConsentToAll]
      facingAuthorizationChallenges:^(NEXAuthorizationSource authSource, id<NEXAuthorizationChallengeResolver> _Nonnull resolver) {
          switch (authSource) {
              case kNEXAuthorizationSourceLocation:
                   // Show default permissions dialog to user
                   [resolver requestAuthorization];
                   break;
              default:
                   break;
          }
      }
                         completion:nil];
    return YES;
}
/// End CodeSnippet: NumberEightGettingStartedAuthorization

-(void) setConsent:(NSDictionary *)launchOptions {
/// Begin CodeSnippet: ManageConsent
NEXConsentOptions* consent = [NEXConsentOptions withDefault];
[consent setConsent:ALLOW_PROCESSING to:true];
[consent setConsent:ALLOW_STORAGE to:true];
[consent setConsent:ALLOW_PRECISE_GEOLOCATION to:true];
[consent setConsent:ALLOW_USE_FOR_PERSONALISED_CONTENT to:true];

[NEXNumberEight startWithApiKey:@"REPLACE_WITH_DEVELOPER_KEY"
                 launchOptions:launchOptions
                 consentOptions:consent
                     completion:nil];
    
// You can also update consent at any time
[NEXNumberEight setConsentOptions:consent];
/// End CodeSnippet: ManageConsent
}

void basicSubscription() {
/// Begin CodeSnippet: UpdatingGlimpses
NEXNumberEight *ne = [[NEXNumberEight alloc] init];

[ne subscribeToDevicePosition:^(NEXGlimpse<NEXDevicePosition *> * _Nonnull glimpse) {
    NSLog(@"Device Position is %@", glimpse.mostProbable);
}];
/// End CodeSnippet: UpdatingGlimpses
}

void usingActivityGlimpse() {
/// Begin CodeSnippet: UsingActivityGlimpses
NEXNumberEight *ne = [[NEXNumberEight alloc] init];

[ne subscribeToSituation:^(NEXGlimpse<NEXSituation *> * _Nonnull glimpse) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NESituation situation = glimpse.mostProbable.value;

        if (situation.major == NESituationMajorWorking
              && situation.minor == NESituationMinorInAnOffice) {
            NSLog(@"User is working in an office!");
        }
    });
}];
/// End CodeSnippet: UsingActivityGlimpses
}

void functions() {
/// Begin CodeSnippet: OtherGlimpses
NEXNumberEight *ne = [[NEXNumberEight alloc] init];

// Activity
[ne subscribeToActivity:^(NEXGlimpse<NEXActivity *> * _Nonnull glimpse) { }];
// Device Movement
[ne subscribeToDeviceMovement:^(NEXGlimpse<NEXMovement *> * _Nonnull glimpse) { }];
// Device Position
[ne subscribeToDevicePosition:^(NEXGlimpse<NEXDevicePosition *> * _Nonnull glimpse) { }];
// Indoor/Outdoor
[ne subscribeToIndoorOutdoor:^(NEXGlimpse<NEXIndoorOutdoor *> * _Nonnull glimpse) { }];
// Place
[ne subscribeToPlace:^(NEXGlimpse<NEXPlace *> * _Nonnull glimpse) { }];
// Situation
[ne subscribeToSituation:^(NEXGlimpse<NEXSituation *> * _Nonnull glimpse) { }];
// Time
[ne subscribeToTime:^(NEXGlimpse<NEXTime *> * _Nonnull glimpse) { }];
// Weather
[ne subscribeToWeather:^(NEXGlimpse<NEXWeather *> * _Nonnull glimpse) { }];

// All other unnamed Glimpses (refer to the API reference for a full list of topics)
[ne subscribeToTopic:kNETopicLockStatus parameters:nil handler:[[NEXGlimpseHandler alloc ] initWithBlock:^(NEXGlimpse<NEXLockStatus*> * _Nonnull glipmse) {}]];
[ne subscribeToTopic:@"" parameters:nil handler:[[NEXGlimpseHandler alloc ] initWithBlock:^(NEXGlimpse<NEXSensorItem*> * _Nonnull glipmse) {}]];
/// End CodeSnippet: OtherGlimpses
}

void usingGlimpses() {
/// Begin CodeSnippet: UsingGlimpses
NEXNumberEight *ne = [[NEXNumberEight alloc] init];

[ne subscribeToSituation:^(NEXGlimpse<NEXSituation *> * _Nonnull glimpse) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NESituation situation = glimpse.mostProbable.value;

        if (situation.major == NESituationMajorWorking
              && situation.minor == NESituationMinorInAnOffice) {
            NSLog(@"User is working in an office!");
        }
    });
}];
/// End CodeSnippet: UsingGlimpses
}

void filteringGlimpses() {
/// Begin CodeSnippet: ParameterizedGlimpses

NEXNumberEight *ne = [[NEXNumberEight alloc] init];

[ne subscribeToTopic:kNETopicActivity
          parameters:NEXParameters.changesMostProbableOnly
             handler:[NEXGlimpseHandler handlerWithBlock:^(NEXGlimpse<NEXActivity *> * _Nonnull glimpse) {
    // Only called when the most probable activity has changed
}]];

[ne subscribeToTopic:kNETopicActivity
          parameters:NEXParameters.sensitivitySmooth
             handler:[NEXGlimpseHandler handlerWithBlock:^(NEXGlimpse<NEXActivity *> * _Nonnull glimpse) {
    // Updates are smoothed such that abrupt changes do not trigger the callback
}]];

[ne subscribeToTopic:kNETopicActivity
          parameters:[NEXParameters.sensitivityLongTerm and:NEXParameters.significantChange]
             handler:[NEXGlimpseHandler handlerWithBlock:^(NEXGlimpse<NEXActivity *> * _Nonnull glimpse) {
    // Only activities that have lasted for several minutes and whose
    // confidences have changed significantly from the last glimpse will
    // trigger the callback.
}]];
/// End CodeSnippet: ParameterizedGlimpses
}

void multipleSubscriptions() {
/// Begin CodeSnippet: MultipleSubscriptions
NEXNumberEight *manager1 = [[NEXNumberEight alloc] init];
NEXNumberEight *manager2 = [[NEXNumberEight alloc] init];

[manager1 subscribeToSituation:^(NEXGlimpse<NEXSituation *> * _Nonnull glimpse) {
    // Situation subscription #1
}];
[manager1 subscribeToSituation:^(NEXGlimpse<NEXSituation *> * _Nonnull glimpse) {
    // Situation subscription #2
}];

[manager2 subscribeToActivity:^(NEXGlimpse<NEXActivity *> * _Nonnull glimpse) {
    // Activity subscription #1
}];
[manager2 subscribeToSituation:^(NEXGlimpse<NEXSituation *> * _Nonnull glimpse) {
    // Situation subscription #3
}];

[manager1 unsubscribeFromAll];
[manager2 unsubscribeFromSituation];
/// End CodeSnippet: MultipleSubscriptions
}

void getDeviceId() {
/// Begin CodeSnippet: GetDeviceId
[NEXNumberEight deviceID];
/// End CodeSnippet: GetDeviceId
}

void deleteUserData() {
/// Begin CodeSnippet: DeleteUserData
[NEXNumberEight deleteUserData];
/// End CodeSnippet: DeleteUserData
}

void createSnapshotter() {
/// Begin CodeSnippet: CreateDefaultSnapshotter
[NEXNumberEight makeDefaultSnapshotter];
/// End CodeSnippet: CreateDefaultSnapshotter

/// Begin CodeSnippet: GetDefaultSnapshotterFilters
[NEXSnapshotter defaultTopics];
[NEXSnapshotter defaultFilters];
/// End CodeSnippet: GetDefaultSnapshotterFilters

/// Begin CodeSnippet: CreateCustomSnapshotter
[NEXNumberEight makeSnapshotterWithTopics:[[NSSet alloc] initWithObjects:kNETopicActivity, kNETopicPlace, kNETopicReachability, nil]];

[NEXNumberEight makeSnapshotterWithParameters:@{
    kNETopicActivity     : [NEXParameters changesOnly],
    kNETopicPlace        : [NEXParameters changesOnly],
    kNETopicReachability : [NEXParameters changesOnly],
}];
/// End CodeSnippet: CreateCustomSnapshotter


NEXSnapshotter* snapshotter = [NEXNumberEight makeDefaultSnapshotter];
/// Begin CodeSnippet: TakeSnapshot
NEXSnapshot* snapshot = [snapshotter takeSnapshot];

NEXSnapshot* customSnapshot = [snapshotter takeSnapshotWithBlock:^NEXSnapshotPair* _Nullable(NEXGlimpse<__kindof NEXSensorItem *> * _Nonnull glimpse) {
    /// Your own logic here...
    return [[NEXSnapshotPair alloc] initWithKey:glimpse.topic
                                          value:[[NSString alloc] initWithFormat:@"value: %@ confidence:%f", glimpse.mostProbable. description, glimpse.mostProbableConfidence]];
}];
/// End CodeSnippet: TakeSnapshot


/// Begin CodeSnippet: PauseResumeSnapshotter
[snapshotter pause];
[snapshotter resume];
/// End CodeSnippet: PauseResumeSnapshotter

/// Begin CodeSnippet: FormatSnapshot
NSLog(@"%@", [snapshot toString]);

NSLog(@"%@", [snapshot toStringWithFormatter:^NSString * _Nullable(NEXSnapshot* _Nonnull snapshot) {
    /// Your own logic here...
    return snapshot.topics.description;
}]);
/// End CodeSnippet: FormatSnapshot

/// Begin CodeSnippet: UsingBuiltInFormatter
NEXJSONFormatter* jsonFormatter = [[NEXJSONFormatter alloc] init];
NSLog(@"%@", [jsonFormatter stringFromSnapshot:snapshot]);

NEXQueryStringFormatter* queryStringFormatter = [[NEXQueryStringFormatter alloc] init];
NSLog(@"%@", [queryStringFormatter stringFromSnapshot:snapshot]);
/// End CodeSnippet: UsingBuiltInFormatter
}

@end
