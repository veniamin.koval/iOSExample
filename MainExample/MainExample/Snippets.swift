//
//  Snippets.swift
//  MainExample
//
//  Created by Chris Watts on 12/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

import Foundation

/// Begin CodeSnippet: ImportNumberEight
import NumberEight
/// End CodeSnippet: ImportNumberEight

/// Begin CodeSnippet: NumberEightGettingStartedAuthorization
func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY",
                      launchOptions: launchOptions,
                      consentOptions: ConsentOptions.withConsentToAll(),
                      facingAuthorizationChallenges: { (authSource, resolver) in
                          switch authSource {
                          case .location:
                               // Show default permisssions dialog to user
                               resolver.requestAuthorization()
                          default:
                               break
                          }
                      })
    return true
}
/// End CodeSnippet: NumberEightGettingStartedAuthorization

func setConsent(launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) {
/// Begin CodeSnippet: ManageConsent
let consent = ConsentOptions.withDefault()
consent.setConsent(.ALLOW_PROCESSING, to: true)
consent.setConsent(.ALLOW_STORAGE, to: true)
consent.setConsent(.ALLOW_PRECISE_GEOLOCATION, to: true)
consent.setConsent(.ALLOW_USE_FOR_PERSONALISED_CONTENT, to: true)

NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY",
                  launchOptions: launchOptions,
                  consentOptions: consent)
    
// You can also update consent at any time
    NumberEight.setConsentOptions(consent)
/// End CodeSnippet: ManageConsent
}

func basicSubscription() {
/// Begin CodeSnippet: UpdatingGlimpses
let ne = NumberEight()

ne.devicePositionUpdated { (glimpse: Glimpse<NEXDevicePosition>) in
    print("Device Position is \(glimpse.mostProbable)")
}
/// End CodeSnippet: UpdatingGlimpses
}


func usingGlimpses() {
/// Begin CodeSnippet: UsingGlimpses
let ne = NumberEight()

ne.situationUpdated { (glimpse: Glimpse<NEXSituation>) in
    DispatchQueue.main.async {
        let situation = glimpse.mostProbable.value

        if situation.major == .working && situation.minor == .inAnOffice {
            print("User is working in an office!")
        }
    }
}
/// End CodeSnippet: UsingGlimpses
}

func usingActivityGlimpse() {
/// Begin CodeSnippet: UsingActivityGlimpses
let ne = NumberEight()

ne.activityUpdated { (glimpse: Glimpse<NEXActivity>) in
    DispatchQueue.main.async {
        if (glimpse.mostProbable.value.modeOfTransport == .car) {
            print("In a car")
        }

        for possibility in glimpse.possibilities {
            let value = possibility.description
            let confidence = possibility.confidence
            print("Possibility: \(value) \(confidence)")
        }
    }
}
/// End CodeSnippet: UsingActivityGlimpses
}


func functions() {
/// Begin CodeSnippet: OtherGlimpses
let ne = NumberEight()

// Activity
ne.activityUpdated { (glimpse: Glimpse<NEXActivity>) in }
// Device Movement
ne.deviceMovementUpdated { (glimpse: Glimpse<NEXMovement>) in }
// Device Position
ne.devicePositionUpdated { (glimpse: Glimpse<NEXDevicePosition>) in }
// Indoor/Outdoor
ne.indoorOutdoorUpdated { (glimpse: Glimpse<NEXIndoorOutdoor>) in }
// Place
ne.placeUpdated { (glimpse: Glimpse<NEXPlace>) in }
// Situation
ne.situationUpdated { (glimpse: Glimpse<NEXSituation>) in }
// Time
ne.timeUpdated { (glimpse: Glimpse<NEXTime>) in }
// Weather
ne.weatherUpdated { (glimpse: Glimpse<NEXWeather>) in }

// All other unnamed Glimpses (refer to the API reference for a full list of topics)
ne.subscribe(to: kNETopicLockStatus, parameters: nil) { (glimpse: Glimpse<NEXLockStatus>) in }
ne.subscribe(to: "", parameters: nil) { (glimpse: Glimpse<NEXSensorItem>) in }
/// End CodeSnippet: OtherGlimpses
}


func parameterizedExamples() {
/// Begin CodeSnippet: ParameterizedGlimpses
let ne = NumberEight()

ne.activityUpdated(parameters: Parameters.changesMostProbableOnly) { (glimpse: Glimpse<NEXActivity>) in
    // Only called when the most probable activity has changed
}

ne.activityUpdated(parameters: Parameters.sensitivitySmooth) { (glimpse: Glimpse<NEXActivity>) in
    // Updates are smoothed such that abrupt changes do not trigger the callback
}

ne.activityUpdated(parameters: Parameters.sensitivityLongTerm.and(Parameters.significantChange)) { (glimpse: Glimpse<NEXActivity>) in
    // Only activities that have lasted for several minutes and whose
    // confidences have changed significantly from the last glimpse will
    // trigger the callback.
}
/// End CodeSnippet: ParameterizedGlimpses
}

func multipleSubscriptions() {
/// Begin CodeSnippet: MultipleSubscriptions
let manager1 = NumberEight()
let manager2 = NumberEight()

manager1.situationUpdated { (glimse) in
    // Situation subscription #1
}.situationUpdated { (glimpse) in
    // Situation subscription #2
}

manager2.activityUpdated { (glimpse) in
    // Activity subscription #1
}.situationUpdated { (glimpse) in
    // Situation subscription #3
}

manager1.unsubscribeFromAll()
manager2.unsubscribeFromSituation()
/// End CodeSnippet: MultipleSubscriptions
}

func getDeviceId() {
/// Begin CodeSnippet: GetDeviceId
NumberEight.deviceID()
/// End CodeSnippet: GetDeviceId
}

func deleteUserData() {
/// Begin CodeSnippet: DeleteUserData
NumberEight.deleteUserData()
/// End CodeSnippet: DeleteUserData
}

func createSnapshotter() {
/// Begin CodeSnippet: CreateDefaultSnapshotter
NumberEight.makeDefaultSnapshotter()
/// End CodeSnippet: CreateDefaultSnapshotter

/// Begin CodeSnippet: GetDefaultSnapshotterFilters
Snapshotter.defaultTopics()
Snapshotter.defaultFilters()
/// End CodeSnippet: GetDefaultSnapshotterFilters

/// Begin CodeSnippet: CreateCustomSnapshotter
NumberEight.makeSnapshotter(withTopics: [kNETopicActivity, kNETopicPlace, kNETopicReachability])

NumberEight.makeSnapshotter(withParameters: [kNETopicActivity : Parameters.changesOnly,
                                                kNETopicPlace : Parameters.changesOnly,
                                         kNETopicReachability : Parameters.changesOnly])

/// End CodeSnippet: CreateCustomSnapshotter


let snapshotter = NumberEight.makeDefaultSnapshotter()
/// Begin CodeSnippet: TakeSnapshot
let snapshot = snapshotter.takeSnapshot()

let customSnapshot = snapshotter.takeSnapshot { (_ glimpse: Glimpse<NEXSensorItem>) -> (String?, Any)? in
    /// Your own logic here...
    return (glimpse.topic, "value:\(glimpse.mostProbable.description) confidence:\(glimpse.mostProbableConfidence)" as Any)
}
/// End CodeSnippet: TakeSnapshot

/// Begin CodeSnippet: PauseResumeSnapshotter
snapshotter.pause()
snapshotter.resume()
/// End CodeSnippet: PauseResumeSnapshotter

/// Begin CodeSnippet: FormatSnapshot
print(snapshot.toString())

print(snapshot.toString(formatter: { snapshot in
    // Your own logic here...
    return snapshot.topics.description
}))
/// End CodeSnippet: FormatSnapshot

/// Begin CodeSnippet: UsingBuiltInFormatter
let jsonFormatter = NEXJSONFormatter()
print(jsonFormatter.string(from: snapshot))

let queryStringFormatter = NEXQueryStringFormatter()
print(queryStringFormatter.string(from: snapshot))
/// End CodeSnippet: UsingBuiltInFormatter
}
