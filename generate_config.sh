#!/bin/bash
set -euo pipefail

if command -v $(which xcodebuild) &> /dev/null; then
	echo "Found Xcode"
else
	echo "Failed to find Xcode.  Install from the App Store"
	exit 1
fi

all_schemes=`xcodebuild -workspace iOSExample.xcworkspace -list | grep -E "^        *" | grep "Example"`
all_configs="Release Debug"
all_sdk="iphonesimulator iphoneos"

IOS_CONFIG_FILE=".ios-gitlab-ci.yml"
rm -f ${IOS_CONFIG_FILE}

for config in $all_configs; do
	for sdk in $all_sdk; do
		for target in $all_schemes; do
			echo "Generating Target:${target}-${config}-${sdk}"
echo \
"build:${target}-${config}-${sdk}:
  tags:
    - macos
  stage: build
  before_script:
    - LANG=en_US.UTF-8 arch -x86_64 pod install
  script:
    - xcodebuild -allowProvisioningUpdates ONLY_ACTIVE_ARCH=NO CONFIGURATION=${config} -workspace iOSExample.xcworkspace -scheme \"${target}\" -sdk \"${sdk}\" build | xcpretty
  interruptible: true
" >> $IOS_CONFIG_FILE
		done
	done
done
