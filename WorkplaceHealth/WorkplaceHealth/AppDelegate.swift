//
//  AppDelegate.swift
//  WorkplaceHealth
//
//  Created by Chris Watts on 13/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: WorkplaceHealthExample
import UIKit
import UserNotifications
import NumberEight

let kNotificationIdentifier = "WorkplaceHealthNotification"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private let ne = NumberEight()
    private var currentPlace: NEXPlace? = nil
    private var currentWeather: NEXWeather? = nil

    private var isSitting = false
    private var activityStart: TimeInterval = 0
    private var inactivityStart: TimeInterval = 0
    private let inactiveThreshold: TimeInterval = 30 * 60 * 1_000 // 30 minutes
    private let changeThreshold: TimeInterval = 5 * 60 * 1_000 // 5 minutes

    private let userNotificationCenter = UNUserNotificationCenter.current()

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY", launchOptions: launchOptions, consentOptions: ConsentOptions.withConsentToAll())

        ne.subscribe(to: kNETopicActivity, parameters: .sensitivitySmooth) { (glimpse : Glimpse<NEXActivity>) in
            DispatchQueue.main.async {
                let state = glimpse.mostProbable.value.state

                if state == .unknown {
                    return
                }

                self.isSitting = state == .stationary || state == .inVehicle
            }
        }.placeUpdated { (glimpse) in
            DispatchQueue.main.async {
                self.currentPlace = glimpse.mostProbable
            }
        }.weatherUpdated { (glimpse) in
            DispatchQueue.main.async {
                self.currentWeather = glimpse.mostProbable
            }
        }

        return true
    }

    func process() {
        let now = Date().timeIntervalSince1970

        if isSitting && inactivityStart == 0 {
            // Mark the start of inactivity
            inactivityStart = now
            print("iOSExample: Now inactive")
        } else if !isSitting && activityStart == 0 {
            // Mark the start of activity
            activityStart = now
            print("iOSExample: Now active")
        }

        let timeActive = now - activityStart
        let timeInactive = now - inactivityStart

        if isSitting && timeInactive > changeThreshold {
            // Mark the end of activity if inactive for longer than the change threshold
            activityStart = 0
        } else if !isSitting && timeActive > changeThreshold {
            // Mark the end of inactivity if active for longer than the change threshold
            inactivityStart = 0
            // Reset the notification
            userNotificationCenter.removeDeliveredNotifications(withIdentifiers: [kNotificationIdentifier])
        }

        // Check if the user has been inactive for too long and they
        // are at work.
        if (isSitting &&
              timeInactive > inactiveThreshold &&
              currentPlace?.value.context.work == .atPlace) {

            // Generate a suggestion based on weather
            var text = "You've been sat at your desk for a while now - "
            if let weather = currentWeather?.value {
                if weather.conditions == .sunny
                    && weather.temperature.rawValue >= NEWeatherTemperature.warm.rawValue {
                    text += "it's nice outside, how about a walk?"
                } else {
                    text += "maybe walk around the office for a while?"
                }
            } else {
                text += "perhaps walk around for a bit?"
            }

            // Display the text as a notification
            let content = UNMutableNotificationContent()
            content.title = "Workplace Health"
            content.body = text
            content.sound = UNNotificationSound.default
            let request = UNNotificationRequest(identifier: kNotificationIdentifier, content: content, trigger: nil)
            userNotificationCenter.add(request)
        }
    }
}
/// End CodeSnippet: WorkplaceHealthExample
