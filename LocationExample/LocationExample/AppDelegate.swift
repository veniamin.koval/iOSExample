//
//  AppDelegate.swift
//  LocationExample
//
//  Created by Matthew Paletta on 2021-11-10.
//

import UIKit
import NumberEight

/// Begin CodeSnippet: GPSLocationExample
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    private let ne = NumberEight()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        /**
         * The @b facingAuthorizationChallenge is an optional parameter.
         *
         * You can use this parameter to control when NumberEight will ask for location permissions.
         * If you do not provide this parameter, but the App `Info.plist` specifies a usage of Location Permissions, a default
         * authorization challenge will be provided, similar to the one shown below, and the Location Permissions prompt will be
         * shown when `NumberEight.start` is first called.
         */
        let _ = NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY", launchOptions: launchOptions, consentOptions: ConsentOptions.withConsentToAll(), facingAuthorizationChallenges: { (authSource, resolver) in
            switch authSource {
            case .location:
                resolver.requestAuthorization()
            @unknown default:
                break
            }
        }) { success, error in
            if (success) {
                print("NumberEight start successfully.")
            } else if let e = error {
                print("NumberEight failed to start with error: \(e.localizedDescription)")
            } else {
                print("NumberEight failed to start with an unknown error.")
            }
        }

        print("iOSExample: Waiting for location updates...")
        ne.subscribe(to: kNETopicLocation) {
            (glimpse : Glimpse<NEXLocation>) in
            let location = glimpse.mostProbable.value
            print("Got Location Update: \(location.coordinate.latitude), \(location.coordinate.longitude)")
        }
        ne.subscribe(to: kNETopicLowPowerLocation) {
            (glimpse : Glimpse<NEXLocation>) in
            let location = glimpse.mostProbable.value
            print("Got Low Power Location Update: \(location.coordinate.latitude), \(location.coordinate.longitude)")
        }

        return true
    }
}
/// End CodeSnippet: GPSLocationExample
