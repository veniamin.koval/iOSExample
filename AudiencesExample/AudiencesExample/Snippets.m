//
//  Snippets.m
//  AudiencesExample
//
//  Created by Chris Watts on 24/09/2020.
//  Copyright © 2020 NumberEight. All rights reserved.
//

#import <Foundation/Foundation.h>
/// Begin CodeSnippet: ImportAudiences
@import Audiences;
/// End CodeSnippet: ImportAudiences

@implementation MyInsightsAppObjC : UIViewController
/// Begin CodeSnippet: InitializeAudiences
-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Start the SDK
    NEXAPIToken* token = [NEXNumberEight startWithApiKey:@"REPLACE_WITH_DEVELOPER_KEY"
                                           launchOptions:launchOptions
                                          consentOptions:[NEXConsentOptions withConsentToAll]
                                              completion:nil];
    // Start recording audiences
    [NEXAudiences startRecordingWithApiToken:token];
    return YES;
}
/// End CodeSnippet: InitializeAudiences

/// Begin CodeSnippet: GetAudiences
-(void)getAudiences {
    NSSet *audiences = [NEXAudiences currentMemberships];
    for (NEXMembership* membership in audiences) {
        NSLog(@"Audiences %@", membership.name);
    }
}

-(void)getAudienceIds {
    NSSet *idList = [NEXAudiences currentIds];
    NSLog(@"Audiences %@", idList); // e.g. ["NE-1-1", "NE-6-2"]
}

-(void)getLiveAudiences {
    NSSet *audiences = [NEXAudiences currentMemberships];
    for (NEXMembership* membership in audiences) {
        if (membership.liveness == kNEXLivenessStateLive) {
            NSLog(@"Live audiences %@", membership.name);
        }
    }
}
/// End CodeSnippet: GetAudiences
@end
