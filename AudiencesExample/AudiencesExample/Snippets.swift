//
//  Snippets.swift
//  AudiencesExample
//
//  Created by Chris Watts on 24/09/2020.
//  Copyright © 2020 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: ImportAudiences
import Audiences
/// End CodeSnippet: ImportAudiences


class MyInsightsAppSwift : UIViewController {
/// Begin CodeSnippet: InitializeAudiences
func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    // Start the SDK
    let token = NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY",
                                  launchOptions: launchOptions,
                                  consentOptions: ConsentOptions.withConsentToAll())

    // Start recording audiences
    Audiences.startRecording(apiToken: token)
    return true
}
/// End CodeSnippet: InitializeAudiences

/// Begin CodeSnippet: GetAudiences
func getAudiences() {
    let audiences = Audiences.currentMemberships
    for membership in audiences {
        NSLog("Audiences \(membership.name)")
    }
}

func getAudienceIds() {
    let idList = Audiences.currentIds
    NSLog("Audiences", idList) // e.g. ["NE-1-1", "NE-6-2"]
}

func getLiveAudiences() {
    let audiences = Audiences.currentMemberships.filter { $0.liveness == .live }
    for membership in audiences {
        NSLog("Live audiences \(membership.name)")
    }
}
/// End CodeSnippet: GetAudiences
}
