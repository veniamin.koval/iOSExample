//
//  AudiencesExampleNoIntegrateApp.swift
//  Shared
//
//  Created by Matthew Paletta on 2021-01-27.
//  Copyright © 2021 NumberEight. All rights reserved.
//

import SwiftUI

@main
struct AudiencesExampleNoIntegrateApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
