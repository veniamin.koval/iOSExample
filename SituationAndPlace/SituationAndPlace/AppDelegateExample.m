//
//  Snippets.m
//  MainExample
//
//  Created by Chris Watts on 12/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: SituationAndPlaceExample
@import NumberEight;

#import <UIKit/UIKit.h>


@interface AppDelegateExample : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) NEXNumberEight *ne;
@property (atomic, strong, nullable) NEXSituation *currentSituation;
@property (atomic, strong, nullable) NEXPlace *currentPlace;

@end

@implementation AppDelegateExample

-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.ne = [[NEXNumberEight alloc] init];

    [NEXNumberEight startWithApiKey:@"REPLACE_WITH_DEVELOPER_KEY"
                      launchOptions:launchOptions
                     consentOptions:[NEXConsentOptions withConsentToAll]
                         completion:nil];

    NSLog(@"iOSExample: Waiting for situation and place updates...");
    [self.ne subscribeToTopic:kNETopicSituation
                   parameters:NEXParameters.changesMostProbableOnly
                      handler:[NEXGlimpseHandler handlerWithBlock:^(NEXGlimpse<NEXSituation *> * glimpse) {
        self.currentSituation = glimpse.mostProbable;
        [self process];
    }]];
    [self.ne subscribeToTopic:kNETopicPlace
                   parameters:NEXParameters.changesMostProbableOnly
                      handler:[NEXGlimpseHandler handlerWithBlock:^(NEXGlimpse<NEXPlace *> * glimpse) {
        self.currentPlace = glimpse.mostProbable;
        [self process];
     }]];

    return YES;
}

-(void)process {
    if (self.currentSituation == nil || self.currentPlace == nil) {
        return;
    }
    NSLog(@"iOSExample: %@, %@", self.currentSituation, self.currentPlace);

    self.currentSituation = nil;
    self.currentPlace = nil;
}

@end
/// End CodeSnippet: SituationAndPlaceExample
