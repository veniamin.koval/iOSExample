//
//  AppDelegate.swift
//  SituationAndPlace
//
//  Created by Chris Watts on 13/12/2018.
//  Copyright © 2018 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: SituationAndPlaceExample
import UIKit
import NumberEight

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    private let ne = NumberEight()
    private var currentSituation: NEXSituation? = nil
    private var currentPlace: NEXPlace? = nil

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY", launchOptions: launchOptions, consentOptions: ConsentOptions.withConsentToAll())

        print("iOSExample: Waiting for situation and place updates...")
        ne.subscribe(to: kNETopicSituation, parameters: .changesMostProbableOnly) { (glimpse : Glimpse<NEXSituation>) in
            self.currentSituation = glimpse.mostProbable
            self.process()
        }.subscribe(to: kNETopicPlace, parameters: .changesMostProbableOnly) { (glimpse : Glimpse<NEXPlace>) in
            self.currentPlace = glimpse.mostProbable
            self.process()
        }

        return true
    }

    private func process() {
        guard
            let situation = currentSituation,
            let place = currentPlace
        else {
            return
        }

        print("iOSExample: \(situation.description) \(place.description)")

        currentSituation = nil
        currentPlace = nil
    }
}
/// End CodeSnippet: SituationAndPlaceExample
