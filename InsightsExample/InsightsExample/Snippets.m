//
//  Snippets.m
//  InsightsExample
//
//  Created by Chris Watts on 25/02/2020.
//  Copyright © 2020 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: ImportInsights
@import Insights;
/// End CodeSnippet: ImportInsights

#import <UIKit/UIKit.h>

@interface MyInsightsAppObjC : UIResponder <UIApplicationDelegate>

@end

@implementation MyInsightsAppObjC
/// Begin CodeSnippet: InitializeInsights
-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NEXAPIToken* token = [NEXNumberEight startWithApiKey:@"REPLACE_WITH_DEVELOPER_KEY"
                                           launchOptions:launchOptions
                                         consentOptions:[NEXConsentOptions withConsentToAll]
                                             completion:nil];
	// Start recording insights
    [NEXInsights startRecordingWithApiToken:token];
    return YES;
}
/// End CodeSnippet: InitializeInsights
@end

/// Begin CodeSnippet: InsightsMarkerEvents
@interface MyInsightsViewController : UIViewController
@end

@implementation MyInsightsViewController

-(void)viewDidLoad:(BOOL)animated {
    [super viewDidLoad];
    [NEXInsights addMarker:@"screen_viewed"];
}

-(void)purchaseMade:(int)value {
    [NEXInsights addMarker:@"in_app_purchase"
                parameters:@{
                    @"value": @(value)
                }];
}

-(void)songPlayedEntitled:(NSString*)title withArtist:(NSString*)artist withGenre:(NSString*)genre {
    [NEXInsights addMarker:@"song_played"
                parameters:@{
                    @"title": title,
                    @"artist": artist,
                    @"genre": genre
                }];
}

@end
/// End CodeSnippet: InsightsMarkerEvents

@interface InsightsSnippets : UIViewController
@end

@implementation InsightsSnippets
-(void)useDeviceId:(NEXAPIToken*)token {
/// Begin CodeSnippet: InsightsSpecifyDeviceID
NEXRecordingConfig* config = [NEXRecordingConfig defaultConfig];
NSString* deviceId = NSUUID.UUID.UUIDString; // this should be saved to the device
config.deviceId = deviceId;

[NEXInsights startRecordingWithApiToken:token config:config];
/// End CodeSnippet: InsightsSpecifyDeviceID
}

-(void)addContexts:(NEXAPIToken*)token {
/// Begin CodeSnippet: InsightsSpecifyConfig
NEXRecordingConfig* config = [NEXRecordingConfig defaultConfig];

// Add the Device Movement context to the list of recorded topics
config.topics = [NSSet setWithArray:@[kNETopicDeviceMovement]];

// Put a smoothing filter on the Device Movement context
NSMutableDictionary *filters = config.filters.mutableCopy;
filters[kNETopicDeviceMovement] = [NEXParameters.sensitivitySmoother and:NEXParameters.changesOnly].filter;
config.filters = filters;

[NEXInsights startRecordingWithApiToken:token config:config];
/// End CodeSnippet: InsightsSpecifyConfig
}

@end
