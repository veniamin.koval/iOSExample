//
//  Snippets.swift
//  InsightsExample
//
//  Created by Chris Watts on 30/10/2019.
//  Copyright © 2019 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: ImportInsights
import Insights
/// End CodeSnippet: ImportInsights

class InitializeSDK : UIViewController {
/// Begin CodeSnippet: InitializeInsights
func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    let token = NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY",
                                  launchOptions: launchOptions,
                                  consentOptions: ConsentOptions.withConsentToAll())
    Insights.startRecording(apiToken: token)
    return true
}
/// End CodeSnippet: InitializeInsights
}

/// Begin CodeSnippet: InsightsMarkerEvents
class MyInsightsAppSwift : UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        Insights.addMarker("screen_viewed")
    }

    func purchaseMade(value: Int) {
        Insights.addMarker("in_app_purchase", parameters: [
            "value": value
        ])
    }

    func songPlayed(title: String, artist: String, genre: String) {
        Insights.addMarker("song_played", parameters: [
            "title": title,
            "artist": artist,
            "genre": genre
        ])
    }
}
/// End CodeSnippet: InsightsMarkerEvents

func useDeviceId(token: APIToken) {
/// Begin CodeSnippet: InsightsSpecifyDeviceID
var config = RecordingConfig.default
let deviceId = UUID().uuidString // this should be saved to the device
config.deviceId = deviceId

Insights.startRecording(apiToken: token, config: config)
/// End CodeSnippet: InsightsSpecifyDeviceID
}


func addContexts(token: APIToken) {
/// Begin CodeSnippet: InsightsSpecifyConfig
var config = RecordingConfig.default

// Add the Device Movement context to the list of recorded topics
config.topics.insert(kNETopicDeviceMovement)

// Put a smoothing filter on the Device Movement context
let parameters = Parameters.sensitivitySmoother.and(Parameters.changesOnly)
config.filters[kNETopicDeviceMovement] = parameters.filter

Insights.startRecording(apiToken: token, config: config)
/// End CodeSnippet: InsightsSpecifyConfig
}
